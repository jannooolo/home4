import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

  //Main meetod. Siin saab teha teste
   public static void main (String[] param) {

      Lfraction b = new Lfraction(1, 5);
      Lfraction c = new Lfraction(1, 3);

      System.out.println(b.equals(c));
       System.out.println(valueOf("3/-6"));
   }

   //Muutujad
   private long nimetaja;
   private long lugeja;



   //murru valmistamine
   public Lfraction (long a, long b) {
      if (b == 0) {
         throw new RuntimeException("nimetaja ei tohi võrduda nulliga");
      }else{
         lugeja = a;
         nimetaja = b;

      }
        if(a<0 && b<0){
            lugeja = Math.abs(a);
            nimetaja = Math.abs(b);
        }
   }

//Tagastab lugeja(numenator)
   public long getNumerator() {
      return lugeja;
   }

   //tagastab nimetaja(denimunator=
   public long getDenominator() { 
      return nimetaja;
   }
   /**
    * Greatest common divisor of two given integers.
    *
    * @param a first integer
    * @param b second integer
    * @return GCD(a, b)
    */
   //Võetud kood on pärit õpetaja materjalidest
   //http://enos.itcollege.ee/~jpoial/java/naited/Fraction.java
   private static long gcd(long a, long b) {
      long nim = Math.max(Math.abs(a), Math.abs(b));
      if (nim == 0) throw new ArithmeticException(" zero in gcd ");
      long lug = Math.min(Math.abs(a), Math.abs(b));
      while (lug > 0) {
         a = nim % lug;
         nim = lug;
         lug = a;
      }
      return nim;
   }

   /**
    * Reduce this fraction (and make denominator > 0).
    *
    * @return reduced fraction
    */
   private Lfraction reduce() {

      Lfraction f = null;
      try {
         f = (Lfraction) clone();
      } catch (CloneNotSupportedException e) {
      }
      ;
      if (nimetaja == 0)
         throw new ArithmeticException(" illegal denominator zero ");
      if (nimetaja < 0) {
         f.lugeja = -lugeja;
         f.nimetaja = -nimetaja;
      }
      if (lugeja == 0)
         f.nimetaja = 1;
      else {
         long n = gcd(lugeja, nimetaja);
         f.lugeja = lugeja / n;
         f.nimetaja = nimetaja / n;
      }
      return f;
   }

  //Muudab long tüüpi muutuja stringiks
   @Override
   public String toString() {
      return Long.toString(lugeja) + "/" + Long.toString(nimetaja);
   }

   //Võrdsus test. tagastab true kui on võrdsed
   @Override
   public boolean equals (Object m) {
       if (m instanceof Lfraction) {

           if (this.reduce().lugeja == ((Lfraction) m).reduce().lugeja &&
                   this.reduce().nimetaja == ((Lfraction) m).reduce().nimetaja) {
               return true;
           } else {
               return false;
           }
       } else {
           return false;
       }
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return (String.valueOf(lugeja) + String.valueOf(nimetaja)).hashCode();
   }

   //Murdude Summa, kontrollib kas nimetajad on samad.
   //vastasel juhul teeb samaks;

   public Lfraction plus (Lfraction m) {

      Lfraction sum;

      if (nimetaja == m.nimetaja) {
         sum = new Lfraction(m.lugeja + lugeja, nimetaja);
      } else {
         long lug = lugeja * m.nimetaja;
         m.lugeja = m.lugeja * nimetaja;
         long nim = nimetaja * m.nimetaja;

         sum = new Lfraction(lug + m.lugeja, nim);
      }
      return sum.reduce();
   }




   /** Murdude korrutis
    * veateate esitamisega kui nimetajas on null
    */
   public Lfraction times (Lfraction m) {

      if (m.nimetaja == 0){
         throw new RuntimeException(" nimetaja ei tohi olla 0 ");
      } else {
         return new Lfraction(lugeja * m.lugeja, nimetaja * m.nimetaja).reduce();
      }
   }



   /** pöörab murru teist pidi ehk  n/d muutub d/n.
    **/
   public Lfraction inverse() {

       if (nimetaja == 0) {
           throw new RuntimeException(" nimetaja ei tohi olla 0 ");
       } else {

           int plusMinus = 1;
       if (lugeja < 0)
           plusMinus = -1;
       return new Lfraction(nimetaja * plusMinus, Math.abs(lugeja));
   }
   }

   // Murru vastand
   public Lfraction opposite() {
      return new Lfraction(-lugeja, nimetaja).reduce();
   }

   /**Murdude vahe ehk lahutamine
    */
   public Lfraction minus (Lfraction m) {

      Lfraction miinus;

      if (nimetaja == m.nimetaja) {
         miinus = new Lfraction(lugeja - m.lugeja, nimetaja);
      } else {
         long lug = lugeja * m.nimetaja;
         m.lugeja = m.lugeja * nimetaja;
         long nim = nimetaja * m.nimetaja;

         miinus = new Lfraction(lug - m.lugeja, nim);
      }
      return miinus.reduce();

   }

   /** Jagamine
    */
   public Lfraction divideBy (Lfraction m) {

       if (m.nimetaja == 0) {
       throw new RuntimeException(" nimetaja ei tohi olla 0 ");
   } else {
           long nim = nimetaja * m.lugeja;
           long lug = lugeja * m.nimetaja;

           if (nim < 0 && lug < 0) {
               nim = nim * -1;
               lug = lug * -1;
           }
           return new Lfraction(lug, nim).reduce();
       }
   }

   /** Murru võrdlus
    */
   @Override
   public int compareTo (Lfraction m) {
       long esimene = lugeja * m.nimetaja;
       long teine = m.lugeja * nimetaja;

       if (m.nimetaja == 0) {
           throw new java.lang.RuntimeException(" nimetaja ei tohi olla null ");
       } else {
           if (esimene < teine) {
               return -1;
           }
           else if (esimene == teine) {
               return 0;
           }
           else if (esimene > teine) {
               return 1;
           }
           else {
               throw new ClassCastException("Ei saa võrrelda");
           }
       }

   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
       return new Lfraction(getNumerator(), getDenominator());
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
       Lfraction tmp = reduce();

       return tmp.getNumerator()/tmp.getDenominator();
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
       return new Lfraction(lugeja - nimetaja*integerPart(), nimetaja).reduce();
   }

   /** Double suuruses esitamine
    */
   public double toDouble() {
       return (double)lugeja / (double)nimetaja;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
       if(d <= 0)
           throw new RuntimeException("Nimetaja ei tohi olla 0 või väiksem");
       return new Lfraction((long)Math.ceil(f * d), d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
       if (s.trim().length()==0){
           throw new RuntimeException("tühi string");
       }else {
           StringTokenizer st = new StringTokenizer(s, "/");
           int lug = 0;
           int nim = 1;
           if (st.hasMoreTokens()) {
               lug = Integer.parseInt(st.nextToken().trim());
           } else {
               throw new IllegalArgumentException("Vigane murd " + s);
           }
           if (st.hasMoreTokens()) {
               nim = Integer.parseInt(st.nextToken());
           } else {
               nim = 1;
           }
           if (st.hasMoreTokens()) {
               throw new IllegalArgumentException("Vigane murd " + s);
           }
           if(nim <=0 ){
               throw new IllegalArgumentException("Vigane murd " + s);
           }

           return new Lfraction(lug, nim).reduce();
       }
   }
}

